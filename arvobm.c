#include"arvobm.h"
void criar_arquivo(char *nome, int num){
  TAB *novo =(TAB*) malloc(sizeof(TAB));
  novo -> info = num;
  novo -> esq = INT_MIN;
  novo -> dir = INT_MIN;
  FILE *fp = fopen(nome, "wb");
  fwrite(novo,sizeof(TAB), 1, fp);
  fclose(fp);
  free(novo);
}

void sobreescreva_arquivo(char *nome, TAB *a){
  
  FILE *fp = fopen(nome, "wb");
  if (!fp) printf("erro");
  fwrite(a, sizeof(TAB), 1, fp);
  fclose(fp);
  
}


TAB *pegar_arvore_do_arquivo(char *nome){
  FILE *fp = fopen(nome, "rb");
  if (!fp) return NULL;
  TAB *novo = (TAB*)malloc(sizeof(TAB));
  fread(novo,sizeof(TAB), 1, fp);
  fclose(fp);
  return novo;
}

void inicializar(int num){
  criar_arquivo("raiz",num);
}

void insere_aux(char *nome, int num){
  TAB *a = pegar_arvore_do_arquivo(nome);
  if (!a) return;
  if (num == a->info) return; 
  else if (num < a->info ){
    
    if (a->esq == INT_MIN){
      a->esq = num;
      sobreescreva_arquivo(nome,a);
      nome =(char *) malloc(10);
      sprintf(nome,"%d", num);
      criar_arquivo(nome,num);
      return;

    }else{
      nome = (char *)malloc(10);
      sprintf(nome,"%i",a ->esq);
      insere_aux(nome,num);
      return;
    }
  }

  else{
    if (a->dir == INT_MIN)
    {
      a->dir = num;
      sobreescreva_arquivo(nome, a);
      nome = (char *)malloc(10);
      sprintf(nome, "%d", num);
      criar_arquivo(nome, num);
      return;
    }
    else
    {
      nome = (char *)malloc(10);
      sprintf(nome, "%i", a->dir);
      insere_aux(nome, num);
      return;
    }
  }
}

void imprime_aux(char *nome){
  TAB *a =pegar_arvore_do_arquivo(nome);
  if (!a) return;
  char *nome_aux = (char*)malloc(10);
  sprintf(nome_aux, "%d", a->esq);
  imprime_aux(nome_aux);
  printf("%d\n", a->info);
  sprintf(nome_aux, "%d", a->dir);
  imprime_aux(nome_aux);

}


void imprime(){
  imprime_aux("raiz");
}


void insere(int num){
  insere_aux("raiz", num);
}
